#include "pch.h"

#include <mutex>
#include <boost/python/errors.hpp>

#include "TTDBase.h"
#include "InstrumentationCallbacks.h"
#include "NirvanaVirtualCPU.h"
#include "ExecutionState.h"
#include "Trace.h"

PyObject *g_py_callback_instructionTranslated = NULL;
PyObject *g_py_callback_memoryRead = NULL;
PyObject *g_py_callback_memoryWrite = NULL;
PyObject *g_py_callback_callRets = NULL;
PyObject *g_py_callback_controlFlow = NULL;

VOID Trace::SetInstructionTranslatedCallback(PyObject *callable)
{
	g_py_callback_instructionTranslated = callable;
}

VOID Trace::SetMemoryReadCallback(PyObject *callable)
{
	g_py_callback_memoryRead = callable;
}

VOID Trace::SetMemoryWriteCallback(PyObject *callable)
{
	g_py_callback_memoryWrite = callable;
}

TranslationCallbackResult * InstructionTranslationCallback(TranslationCallbackResult *result, NirvanaVirtualCPU *virtualCpu,
	GuestAddress instructionAddress, UINT64 unknown, UINT64 instructionSize)
{
	ExecutionState *executionState = (ExecutionState *)((BYTE *)virtualCpu->contextMaybe - 128);
	Position currentPosition = executionState->GetPosition();
	currentPosition.steps += virtualCpu->GetInstructionCount();

	/*
	UINT64 registerBuffer = -1;
	virtualCpu->GetRegister(rax, &registerBuffer, sizeof(registerBuffer));
	printf("RAX = %llx \n", registerBuffer);

	char memoryBuffer[500] = { 0 };
	TBufferView bufferView = { 0 };
	bufferView.buffer = &memoryBuffer;
	bufferView.requestedSize = 500;
	MemoryBuffer memoryInfo = executionState->QueryMemoryBuffer((GuestAddress)((UINT64)instructionAddress), bufferView);
	*/

	if (g_py_callback_instructionTranslated != NULL)
	{
		// Initialize and acquire the global interpreter lock
		//PyEval_InitThreads();

		// Ensure that the current thread is ready to call the Python C API
		PyGILState_STATE state = PyGILState_Ensure();

		try
		{
			// invoke the python function
			boost::python::call<void>(g_py_callback_instructionTranslated, currentPosition, (UINT64)instructionAddress, instructionSize);
		}
		catch (const boost::python::error_already_set&) {
			printf("[-] A python exception has occured while executing the instruction translation callback. \n");
			printf("[-] In position: %llx:%llx \n", currentPosition.sequencePoint, currentPosition.steps);
			// TODO: stop executing by calling ResetInstructionCount or so
		}
		// release the global interpreter lock so other threads can resume execution
		PyGILState_Release(state);
	}


	result->userCallback = NULL;
	return result;
}

VOID MemoryReadCallback(NirvanaVirtualCPU *nirvanaVirtualCpu, GuestAddress address, PVOID somePointer, UINT64 readSize)
{
	ExecutionState *executionState = (ExecutionState *)((BYTE *)nirvanaVirtualCpu->contextMaybe - 128);
	Position currentPosition = executionState->GetPosition();
	currentPosition.steps += nirvanaVirtualCpu->GetInstructionCount();

	/*UINT64 rip = nirvanaVirtualCpu->GetPC64();
	printf("RIP = %p (VCPU=%p) \n", rip, nirvanaVirtualCpu);
	printf("Memory at %p with size %llu was read.\n", address, readSize);*/

	if (g_py_callback_memoryRead != NULL)
	{
		PyGILState_STATE state = PyGILState_Ensure();

		try
		{
			// invoke the python function
			boost::python::call<void>(g_py_callback_memoryRead, currentPosition, (UINT64)address, readSize);
		}
		catch (const boost::python::error_already_set&) {
			printf("[-] A python exception has occured while executing the memory read callback. \n");
			printf("[-] In position: %llx:%llx \n", currentPosition.sequencePoint, currentPosition.steps);

			// TODO: stop executing by calling ResetInstructionCount or so
		}

		PyGILState_Release(state);
	}
}

VOID MemoryWriteCallback(NirvanaVirtualCPU *virtualCpu, GuestAddress address, PVOID somePointer, UINT64 writeSize)
{
	ExecutionState *executionState = (ExecutionState *)((BYTE *)virtualCpu->contextMaybe - 128);
	Position currentPosition = executionState->GetPosition();
	currentPosition.steps += virtualCpu->GetInstructionCount();

	//printf("RIP = %p (VCPU=%p) \n", virtualCpu->GetPC64(), virtualCpu);
	//printf("Memory at %p with size %llu was written to.\n", address, writeSize);

	if (g_py_callback_memoryWrite != NULL)
	{
		try
		{
			// Ensure that the current thread is ready to call the Python C API
			PyGILState_STATE state = PyGILState_Ensure();

			// invoke the python function
			boost::python::call<void>(g_py_callback_memoryWrite, currentPosition, (UINT64)address, writeSize);

			PyGILState_Release(state);
		}
		catch (const boost::python::error_already_set&) {
			printf("[-] A python exception has occured while executing the memory write callback. \n");
			printf("[-] In position: %llx:%llx \n", currentPosition.sequencePoint, currentPosition.steps);

			// TODO: stop executing by calling ResetInstructionCount or so
		}
	}
}

