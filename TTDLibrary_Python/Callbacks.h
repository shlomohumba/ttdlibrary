#pragma once
#include "TTDBase.h"
#include "InstrumentationCallbacks.h"

TranslationCallbackResult * InstructionTranslationCallback(TranslationCallbackResult *result, NirvanaVirtualCPU *virtualCpu,
	GuestAddress instructionAddress, UINT64 unknown, UINT64 instructionSize);
VOID MemoryReadCallback(NirvanaVirtualCPU *nirvanaVirtualCpu, GuestAddress address, PVOID somePointer, UINT64 readSize);
VOID MemoryWriteCallback(NirvanaVirtualCPU *virtualCpu, GuestAddress address, PVOID somePointer, UINT64 writeSize);