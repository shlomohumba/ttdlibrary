#pragma once

#include "TTDBase.h"
#include "RegisterId.h"
#include <boost/python.hpp>

class ReplayEngine;
class Cursor;

class Trace
{
public:
	Trace(std::wstring runFilePath);

	Position GetStartingPosition();
	Position GetEndingPosition();

	Position GetCurrentPosition();
	Position SetPosition(Position newPosition);
	Position Go(Position targetPosition = MaxPosition);
	Position GoBackwards(Position targetPosition = MinPosition);
	Position Step();
	Position JumpToStart();
	Position JumpToEnd();

	VOID SetInstructionTranslatedCallback(PyObject *callable);
	VOID SetMemoryReadCallback(PyObject *callable);
	VOID SetMemoryWriteCallback(PyObject *callable);

	UINT64 GetProgramCounter();
	UINT64 GetStackPointer();
	std::string GetMemory(UINT64 address, UINT64 size);
	UINT64 GetRegister(RegisterId registerId);

private:
	ReplayEngine *m_replayEngine;
	Cursor *m_cursor;

	BOOL m_isRunning = FALSE;
	BOOL m_isGoingForward = TRUE;
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(go_overloads, Trace::Go, 0, 1);
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(go_backwards_overloads, Trace::GoBackwards, 0, 1);

