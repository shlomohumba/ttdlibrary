#include "pch.h"
#include <string>

#include "TTDInit.h"
#include "TTDBase.h"
#include "ReplayEngine.h"
#include "Cursor.h"
#include "NirvanaVirtualCPU.h"

#include "Trace.h"
#include "Callbacks.h"

Trace::Trace(std::wstring runFilePath)
{
	if (!boost::filesystem::exists(runFilePath))
	{
		std::string runFileASCII(runFilePath.begin(), runFilePath.end());
		throw std::exception(std::string("Could not find a trace named '" + runFileASCII + "'").c_str());
	}

	m_replayEngine = CreateReplayEngineWithTraceFile(runFilePath.c_str());
	if (m_replayEngine == NULL)
	{
		throw std::exception("Could not load trace file");
	}

	m_cursor = m_replayEngine->NewCursor(&CURSOR_GUID);
	m_cursor->SetPosition(m_replayEngine->GetFirstPosition());

	// Register the callbacks
	GlobalCallbacks callbacks = { 0 };
	callbacks.MemoryReadCallback = MemoryReadCallback;
	callbacks.MemoryWriteCallback = MemoryWriteCallback;
	callbacks.InstructionTranslationCallback = InstructionTranslationCallback;
	RegisterGlobalCallbacks(&callbacks);
}

Position Trace::GetStartingPosition()
{
	return m_replayEngine->GetFirstPosition();
}

Position Trace::GetEndingPosition()
{
	return m_replayEngine->GetLastPosition();
}

Position Trace::GetCurrentPosition()
{
	return m_cursor->GetPosition();
}

Position Trace::SetPosition(Position newPosition)
{
	m_cursor->SetPosition(newPosition);
	return m_cursor->GetPosition();
}

Position Trace::JumpToStart()
{
	m_cursor->SetPosition(m_replayEngine->GetFirstPosition());
	return m_cursor->GetPosition();
}

Position Trace::JumpToEnd()
{
	m_cursor->SetPosition(m_replayEngine->GetLastPosition());
	return m_cursor->GetPosition();
}


UINT64 Trace::GetRegister(RegisterId registerId)
{
	CROSS_PLATFORM_CONTEXT context = m_cursor->GetCrossPlatformContext();

	// TODO: check bounds
	return context.x64.registers[registerId];
}

UINT64 Trace::GetProgramCounter()
{
	return (UINT64)m_cursor->GetProgramCounter();
}

UINT64 Trace::GetStackPointer()
{
	return (UINT64)m_cursor->GetStackPointer();
}

std::string Trace::GetMemory(UINT64 address, UINT64 size)
{
	char *tempBuffer = new char[size];
	
	TBufferView bufferView = { 0 };
	bufferView.buffer = (PVOID)tempBuffer;
	bufferView.requestedSize = size;
	MemoryBuffer memoryInfo2 = m_cursor->QueryMemoryBuffer((GuestAddress)address, bufferView, 0);
	std::string outputString = std::string(tempBuffer, tempBuffer+size);

	delete[] tempBuffer;

	return outputString;
}

Position Trace::Go(Position targetPosition)
{
	m_isRunning = TRUE;
	m_isGoingForward = TRUE;

	// ReplayForward is going to schedule new threads that will execute the VCPU, and then wait for them.
	// If a python callback will be called from one of these threads, it must have the GIL free to take.
	PyGILState_STATE state = PyGILState_Ensure();
	PyGILState_Release(PyGILState_UNLOCKED);
	
	m_cursor->ReplayForward(targetPosition);

	PyGILState_Ensure();
	
	m_isRunning = FALSE;
	m_isGoingForward = FALSE;
	return m_cursor->GetPosition();
}

Position Trace::GoBackwards(Position targetPosition)
{
	m_isRunning = TRUE;
	m_isGoingForward = FALSE;

	// ReplayBackward is going to schedule new threads that will execute the VCPU, and then wait for them.
	// If a python callback will be called from one of these threads, it must have the GIL free to take.
	PyGILState_STATE state = PyGILState_Ensure();
	PyGILState_Release(PyGILState_UNLOCKED);

	m_cursor->ReplayBackward(targetPosition);

	PyGILState_Ensure();


	m_isRunning = FALSE;
	return m_cursor->GetPosition();
}

Position Trace::Step()
{
	m_cursor->ReplayForward(MaxPosition, 1);
	return m_cursor->GetPosition();
}