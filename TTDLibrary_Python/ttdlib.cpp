// dllmain.cpp : Defines the entry point for the DLL application.

#include "pch.h"

#include "TTDBase.h"
#include "Trace.h"
#include "PythonBindings.h"
#include "RegisterId.h"

using namespace boost::python;


BOOST_PYTHON_MODULE(ttdlib)
{
	class_<Trace>("Trace", init<std::wstring>())
		.add_property("starting_position", &Trace::GetStartingPosition, "Gets the first Time-Travel position in the trace")
		.add_property("ending_position", &Trace::GetEndingPosition, "Gets the last Time-Travel position in the trace")
		.add_property("position", &Trace::GetCurrentPosition, "Gets the current Time-Travel position in the trace")
		.def("get_register", &Trace::GetRegister, "Gets the value of the specified x64 register, by ID")
		.def("get_memory", &Trace::GetMemory, "Gets the current memory contents of the specfied address and size\r\n"
			"This function may return less memory than requested, if the rest is not mapped")
		.add_property("stack_pointer", &Trace::GetStackPointer, "Gets the current value of the RSP register (in x64)")
		.add_property("program_counter", &Trace::GetProgramCounter, "Gets the current value of the RIP register (in x64)")
		.def("set_position", &Trace::SetPosition, "Sets the current Time-Travel position")
		.def("jump_to_start", &Trace::JumpToStart, "Sets the current Time-Travel position to the beginning of the trace")
		.def("jump_to_end", &Trace::JumpToEnd, "Sets the current Time-Travel position to the end of the trace")
		.def("step", &Trace::Step, "Advances the current Time-Travel position by one instruction")
		.def("go", &Trace::Go, go_overloads(args("target_position"), "Replays the trace forward, optionally with a target position"))
		.def("go_backwards", &Trace::GoBackwards, go_backwards_overloads(args("target_position"), "Replays the trace backwards, optionally with a target position"))
		.def("set_intruction_translated_callback", &Trace::SetInstructionTranslatedCallback, "Sets the intruction translation callback that will be called every time an instruction is run\r\n"
																							 "The callback will receive 3 arguments:\n1.Position\r\n2.Instruction address\r\n3.Instruction Size")
		.def("set_memory_read_callback", &Trace::SetInstructionTranslatedCallback, "Sets the memory read callback that will be called every time an instruction fetches memory\r\n"
																					"The callback will receive 3 arguments:\n1.Position\r\n2.Read address\r\n3.Read size")
		.def("set_memory_write_callback", &Trace::SetInstructionTranslatedCallback, "Sets the memory write callback that will be called every time an instruction writes to memory\r\n"
																					"The callback will receive 3 arguments:\n1.Position\r\n2.Write address\r\n3.write size")

		;

	class_<Position>("Position", init<UINT64, UINT64>())
		.def_readonly("sequence", &Position::sequencePoint)
		.def_readonly("steps", &Position::steps)
		.def("__repr__", &getPythonRepr);

	enum_<RegisterId>("RegisterId")
		.value("something1", something1)
		.value("something2", something2)
		.export_values()
		.value("something3", something3)
		.value("something4", something4)
		.value("something5", something5)
		.value("something6", something6)
		.value("rax", rax)
		.value("rcx", rcx)
		.value("rdx", rdx)
		.value("rbx", rbx)
		.value("rsp", rsp)
		.value("rbp", rbp)
		.value("rsi", rsi)
		.value("rdi", rdi)
		.value("r8", r8)
		.value("r9", r9)
		.value("r10", r10)
		.value("r11", r11)
		.value("r12", r12)
		.value("r13", r13)
		.value("r14", r14)
		.value("r15", r15)
		.value("rip", rip)
		.value("cs", cs)
		.value("ss", ss)
		.value("ds", ds)
		.value("fs", fs)
		.value("gs", gs)
		;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}

