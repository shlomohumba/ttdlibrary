#include "pch.h"
#include <string>
#include <boost/format.hpp>

#include "PythonBindings.h"

std::string getPythonRepr(Position pos)
{
	return boost::str(boost::format("< Position %X:%X >") % pos.sequencePoint % pos.steps);
}