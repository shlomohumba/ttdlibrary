#include "pch.h"
#include <Windows.h>
#include <string>
#include <stdio.h>
#define INITGUID
#include <guiddef.h>
#include <iostream>

#include "TTDInit.h"
#include "TTDBase.h"
#include "ReplayEngine.h"
#include "InstrumentationCallbacks.h"


// GUID_34bee39e_87c1_417e_a8df_d0b0354cfa9d
DEFINE_GUID(TTD_SECRET_GUID, 0x34bee39e, 0x87c1, 0x417e, 0xa8, 0xdf, 0xd0, 0xb0, 0x35, 0x4c, 0xfa, 0x9d);
const char *TTDSecretPort2 = "2tAktJpVVT2xTuUvpccLMdKHlV3cV1Gl0WuDBqqF8ro";

const wchar_t *TTD_REPLAY_PATH = L"TTDReplay.dll";
const wchar_t *TTD_REPLAY_CPU_PATH = L"TTDReplayCPU.dll";

typedef UINT32(_fastcall *createReplayEngineFn)(const char * secretPortBase64, PVOID outputReplayEngine, PVOID guid);
typedef UINT32(_fastcall *RegisterInstrumentationCallbacksFn)(GlobalCallbacks *);


ReplayEngine *CreateReplayEngineWithTraceFile(const wchar_t *runFilePath)
{
	HMODULE ttdModule = LoadLibraryW(TTD_REPLAY_PATH);
	if (!ttdModule)
	{
		printf("[-] Couldn't load the TTDReplay.dll library. Error code: %d \n", GetLastError());
		return NULL;
	}

	PVOID createReplayEnginePointer = GetProcAddress(ttdModule, "CreateReplayEngineWithHandshake");
	if (!createReplayEnginePointer)
	{
		printf("[-] Couldn't find the CreateReplayEngineWithHandshake function. Error code: %d \n", GetLastError());
		return NULL;
	}

	// TOOD: In the future we will call this function to generate a real handshake
	PVOID initiateReplayEngineHandshakePointer = GetProcAddress(ttdModule, "InitiateReplayEngineHandshake");

	// Patch the handshake check
	BYTE *check = (BYTE *)createReplayEnginePointer + 0x18C;
	if (*check == 0x74)
	{
		// pacth it
		DWORD oldProtect = 0;
		BOOL success = VirtualProtect(check, 4, PAGE_EXECUTE_READWRITE, &oldProtect);
		if (!success)
		{
			printf("[-] Could not VirtualProtect to patch the handshake check. Error: %d \n", GetLastError());
			return NULL;
		}
		*check = 0x75;
	}
	else
	{
		printf("[-] Could not find the handshake check. Please check the offsets.\n");
		return NULL;
	}

	createReplayEngineFn CreateReplayEngineWithHandshake = (createReplayEngineFn)createReplayEnginePointer;
	UINT32 status = -1;

	ReplayEngine *replayEngine = NULL;
	status = CreateReplayEngineWithHandshake("Base64HandshakeToGenerate", &replayEngine, (PVOID)&TTD_SECRET_GUID.Data1);
	if (status != 0)
	{
		printf("[-] Failed to create replay engine. code: %d \n", status);
		return NULL;
	}

	//printf("[+] Initiating ReplayEngine with run file... \n", replayEngine);
	status = replayEngine->Initialize(runFilePath);
	if (status != 1)
	{
		printf("[-] Failed to initialize ReplayEngine. status: %d \n", status);
		return NULL;
	}
	// printf("[+] OK \n");

	return replayEngine;
}

UINT32 RegisterGlobalCallbacks(GlobalCallbacks * callbacks)
{
	HMODULE ttdModule = LoadLibraryW(TTD_REPLAY_CPU_PATH);
	if (!ttdModule)
	{
		printf("[-] Couldn't load the TTDReplayCPU.dll library. Error code: %d \n", GetLastError());
		return 1;
	}

	PVOID registerInstrumentationCallbacksPointer = GetProcAddress(ttdModule, "RegisterInstrumentationCallbacks");
	if (!registerInstrumentationCallbacksPointer)
	{
		printf("[-] Couldn't find the RegisterInstrumentationCallbacks function. Error code: %d \n", GetLastError());
		return 1;
	}

	RegisterInstrumentationCallbacksFn RegisterInstrumentationCallbacks = (RegisterInstrumentationCallbacksFn)registerInstrumentationCallbacksPointer;
	return RegisterInstrumentationCallbacks(callbacks);
}
