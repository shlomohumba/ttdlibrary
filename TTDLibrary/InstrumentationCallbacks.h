#pragma once
#include <Windows.h>

#include "TTDBase.h"

class NirvanaVirtualCPU;

typedef struct
{
	PVOID ControlFlowCallback;
	PVOID CallRetsCallback;
	PVOID InstructionTranslationCallback;
	PVOID MemoryWriteCallback;
	PVOID MemoryReadCallback;
	PVOID DebugBreakCallback;
	PVOID AtomicOpCallback;
	PVOID MessageCallback;

} GlobalCallbacks;

typedef struct
{
	PVOID userCallback;
	UINT64 parameter;

} TranslationCallbackResult;;

typedef struct
{
	VOID (_fastcall *MemoryWriteCallback)(NirvanaVirtualCPU *nirvanaVirtualCpu, GuestAddress address, PVOID, UINT64 readSize);
	VOID (_fastcall *MemoryReadCallback)(NirvanaVirtualCPU *nirvanaVirtualCpu, GuestAddress address, PVOID, UINT64 writeSize);
	TranslationCallbackResult * (_fastcall *InstructionTranslationCallback)(TranslationCallbackResult *translationCallbackResult, NirvanaVirtualCPU *virtualCpu, 
																			GuestAddress instructionAddress, UINT64 unknown, UINT64 instructionSize);
	PVOID MemoryEvictCallback;

} ContextCallbacks;

typedef struct
{
	PVOID MemoryFetchCallback;
	PVOID TimestampCallback;

} Callbacks;
