#pragma once
#include <Windows.h>

#include "TTDBase.h"

class NirvanaVirtualCPU;

class ActiveGuestContext
{
	UINT64 threadIdMaybe;
	UINT64 unknown1;
	UINT64 unknown2;
	PVOID somePointer;
	PVOID VcpuPool;
public:
	NirvanaVirtualCPU *nirvannaSmartContext;
	BYTE moreData[3320];
};