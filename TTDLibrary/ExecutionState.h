#pragma once
#include <Windows.h>

#include "TTDBase.h"
#include "ActiveGuestContext.h"
#include "GuestContext.h"

class ExecutionState
{
	BYTE stuff[128 - sizeof(PVOID)];
	GuestContext guestContext;		 // offset +128 (GuestContext)
	BYTE moreStuff1[464];			 // offset +3456
	Position currentPosition;		 // offset +3920
	BYTE moreStuff2[88];			 // offset +3936
	Position somePosition;			 // offset +4024
	BYTE moreStuff[264];			 // offset +4040
	UINT64 hasBreakpoints;			// offset +4304
public:
	ActiveGuestContext activeGuestContext;

	virtual PVOID GetThreadInfo(void);
	virtual UINT32 GetTebAddress(void);
	virtual Position& GetPosition(void);
	virtual UINT32 GetPreviousPosition(void);
	virtual UINT32 GetProgramCounter(void);
	virtual UINT32 GetStackPointer(void);
	virtual UINT32 GetCrossPlatformContext(void);
	virtual UINT32 GetAvxExtendedContext(void);
	virtual UINT32 QueryMemoryRange(GuestAddress);
	virtual MemoryBuffer QueryMemoryBuffer(GuestAddress, TBufferView bufferView);
	virtual UINT32 QueryMemoryBufferWithRanges(GuestAddress, PVOID, unsigned __int64, MemoryRange *);
	virtual ~ExecutionState();
};