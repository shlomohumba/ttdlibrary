#pragma once
#include <Windows.h>

#include "TTDBase.h"

class Cursor;

class ReplayEngine
{
public:
	virtual UINT32 UnsafeAsInterface(_GUID const &);
	//virtual void ReplayEngine(void);
	virtual UINT32 UnsafeAsInterface2(_GUID const &);
	virtual UINT32 GetPebAddress(void);
	virtual PVOID GetSystemInfo(void);
	virtual Position & GetFirstPosition(void);
	virtual Position & GetLastPosition(void);
	virtual Position & GetFirstPosition2(void);
	virtual UINT32 GetRecordingType(void);
	virtual ThreadInfo GetThreadInfo(UniqueThreadId);
	virtual UINT32 GetThreadCount(void);
	virtual ThreadInfo * GetThreadList(void);
	virtual UINT32 GetThreadFirstPositionIndex(void);
	virtual UINT32 GetThreadLastPositionIndex(void);
	virtual UINT32 GetThreadLifetimeFirstPositionIndex(void);
	virtual UINT32 GetThreadLifetimeLastPositionIndex(void);
	virtual UINT32 GetThreadCreatedEventCount(void);
	virtual UINT32 GetThreadCreatedEventList(void);
	virtual UINT32 GetThreadTerminatedEventCount(void);
	virtual PVOID GetThreadTerminatedEventList(void);
	virtual UINT32 GetModuleCount(void);
	virtual UINT32 GetModuleList(void);
	virtual UINT32 GetModuleInstanceCount(void);
	virtual PVOID GetModuleInstanceList(void);
	virtual UINT32 GetModuleInstanceUnloadIndex(void);
	virtual UINT32 GetModuleLoadedEventCount(void);
	virtual PVOID GetModuleLoadedEventList(void);
	virtual UINT32 GetModuleUnloadedEventCount(void);
	virtual PVOID GetModuleUnloadedEventList(void);
	virtual UINT32 GetExceptionEventCount(void);
	virtual PVOID GetExceptionEventList(void);
	virtual PVOID GetExceptionAtOrAfterPosition(Position const &);
	virtual UINT32 GetKeyframeCount(void);
	virtual PVOID GetKeyframeList(void);
	virtual UINT32 GetRecordClientCount(void);
	virtual PVOID GetRecordClientList(void);
	virtual PVOID GetRecordClient(RecordClientId);
	virtual UINT32 GetCustomEventCount(void);
	virtual PVOID GetCustomEventList(void);
	virtual UINT32 GetActivityCount(void);
	virtual PVOID GetActivityList(void);
	virtual UINT32 GetIslandCount(void);
	virtual PVOID GetIslandList(void);
	virtual Cursor * NewCursor(GUID const *);
	//virtual UINT32 BuildIndex(void(*)(void const *, TTD::Replay::IndexBuildProgressType const *), void const *, TTD::Replay::IndexBuildFlags);
	virtual UINT32 BuildIndex();
	virtual UINT32 GetIndexStatus(void);
	virtual UINT32 GetIndexFileStats(void);
	//virtual UINT32 RegisterErrorAndWarningCallbacks(void(*)(wchar_t const *), void(*)(TTD::Replay::LoggingInformation const &, TTD::Replay::DebugEvent, wchar_t const *), TTD::Replay::DebugModeType, TTD::ErrorReporting *);
	virtual UINT32 RegisterErrorAndWarningCallbacks();
	virtual UINT32 GetInternals(void);
	virtual UINT32 GetInternals2(void);
	virtual ~ReplayEngine();
	virtual UINT32 Destroy(void);
	virtual UINT32 Initialize(wchar_t const *);
};