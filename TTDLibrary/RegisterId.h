#pragma once

enum RegisterId
{
	something1 = 0,
	something2 = 1,
	something3 = 2,
	something4 = 3,
	something5 = 4,
	something6 = 5,
	rax = 6,
	rcx,
	rdx,
	rbx,
	rsp,
	rbp,
	rsi,
	rdi,
	r8,
	r9,
	r10,
	r11,
	r12,
	r13,
	r14,
	r15,
	rip,
	cs, // ??
	ss, // ??
	ds, // ??
	fs, // ??
	gs, // ??
};