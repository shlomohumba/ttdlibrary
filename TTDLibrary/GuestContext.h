#pragma once
#include <Windows.h>

#include "TTDBase.h"

class GuestContext
{
	PVOID somePointer;		// offset +0
	UINT64 someOffset;		// offset +8
	BYTE something[112];	// offset + 16
	BYTE data[3200];		// offset + 128
};