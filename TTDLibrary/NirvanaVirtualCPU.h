#pragma once
#include <Windows.h>

#include "TTDBase.h"
#include "InstrumentationCallbacks.h"
#include "GuestContext.h"
#include "RegisterId.h"

typedef LONG SCODE; // not sure what's this yet

class NirvanaVirtualCPU
{
public:
	GuestContext * contextMaybe;
	ContextCallbacks registeredInstrumentationCallbacks;
	ContextCallbacks instrumentationCallbacks;
	Callbacks replayCallbacks;

public:
	virtual UINT32 SetClientTls(void *);
	virtual UINT32 GetRegistersData(void);
	virtual UINT32 GetVirtualProcessorState(UINT32, void * const);
	virtual UINT32 SetVirtualProcessorState(UINT32, void const * const);
	virtual UINT32 GetRegister(RegisterId registerId, void *const outputBuffer, unsigned __int64 bufferSize);
	virtual UINT32 GetPC32(void);
	virtual UINT64 GetPC64(void);
	virtual UINT32 GetTeb32(void);
	virtual UINT64 GetTeb64(void);
	virtual UINT32 GetRegistersHash(SelectRegisters);
	virtual UINT32 GetInstructionCount(void);
	virtual UINT32 ResetInstructionCount(void);
	virtual UINT32 SetInstructionCountLimit(UINT32);
	virtual UINT32 ResetCaches(void);
	virtual UINT32 QueryDataCacheLineSize(void);
	virtual UINT32 AlignAddrToDataCacheLineSize(GuestAddress);
	virtual UINT32 RemoveLineFromDataCache(GuestAddress);
	virtual UINT32 RemoveRangeFromCodeCache(GuestAddress, unsigned __int64);
	virtual UINT32 ReadLineFromDataCache(void *, GuestAddress, MemoryTag &, unsigned __int64);
	virtual UINT32 WriteGuestMemory(GuestAddress, void const *, unsigned __int64);
	virtual UINT32 CreateOrUpdateCacheLine(GuestAddress, void const *, unsigned __int64, CacheLineFlags);
	virtual UINT32 FindFirstValidDataLine(void const * * const, GuestAddress &, MemoryTag &, unsigned __int64, FindValidDataLineMode);
	virtual UINT32 FindNextValidDataLine(void const * * const, GuestAddress &, MemoryTag &, unsigned __int64, FindValidDataLineMode);
	virtual UINT32 SetMemoryTag(MemoryTag);
	virtual UINT32 RegisterInstrumentationCallbacks(ContextCallbacks const &);
	virtual UINT32 RegisterReplayCallbacks(Callbacks const &);
	virtual UINT32 SyncGuestMachineState(UINT32);
	virtual UINT32 DisableFastMemoryPath(void);
	virtual UINT32 EnableFastMemoryPath(void);
	virtual ~NirvanaVirtualCPU();
	virtual UINT32 DispatcherLoop(SCODE *);
	virtual UINT32 AcquireVirtualContext(EmulatorRegisters const *);
	virtual UINT32 RealizeState(void);
	virtual UINT32 VirtualizeState(void);
	virtual UINT32 ResetCaches2(void);
	virtual UINT32 RemovePotentialFallThroughOnPreviousScode(SCODE *);
	virtual UINT32 DeleteRangeFromScodeCache(GuestAddress, unsigned __int64);
	virtual UINT32 GetStackPointer(void);
	virtual UINT32 SetClientTls2(void *);
};