#pragma once

#include "TTDBase.h"
#include "InstrumentationCallbacks.h"

class ReplayEngine;
class NirvanaVirtualCPU;

void testTTD();

VOID MemoryReadCallback(NirvanaVirtualCPU *nirvanaVirtualCpu, GuestAddress address, PVOID somePointer, UINT64 sizeMaybe);
VOID MemoryWriteCallback(NirvanaVirtualCPU *nirvanaVirtualCpu, GuestAddress address, PVOID somePointer, UINT64 sizeMaybe);
TranslationCallbackResult * InstructionTranslationCallback(TranslationCallbackResult *translationCallbackResult, NirvanaVirtualCPU *virtualCpu, GuestAddress instructionAddress, UINT64 unknown, UINT64 instructionSize);