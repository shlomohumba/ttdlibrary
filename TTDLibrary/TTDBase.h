#pragma once

#pragma once
#include <Windows.h>

typedef UINT64 UniqueThreadId;
typedef UINT64 RecordClientId;
typedef PVOID GuestAddress;
typedef UINT64 QueryMemoryPolicy;
typedef UINT64 ThreadId;
typedef UINT64 UniqueThreadId;
typedef UINT64 EventMask;
typedef UINT64 ExceptionMask;
typedef UINT64 ReplayFlags;
typedef UINT64 StepCount;
typedef UINT64 SequenceId;
typedef BYTE SelectRegisters;
typedef UINT64 MemoryTag;
typedef UINT64 CacheLineFlags;
typedef BYTE FindValidDataLineMode;

#define CURRENT_TID (0)

typedef struct
{
	GuestAddress address;
	UINT64 dataAccessMask;
	BYTE something;

} WatchpointData;

typedef struct Position
{
public:
	Position(SequenceId sequence, StepCount step) : sequencePoint(sequence), steps(step) {};

	SequenceId sequencePoint;
	StepCount steps;

} Position;

static Position MaxPosition = { 0x0FFFFFFFFFFFFFFFE, 0x0FFFFFFFFFFFFFFFE };
static Position MinPosition = { 0x0, 0x0 };

typedef struct
{
	UINT64 thing1;
	UINT64 thing2;
	UINT64 thing3;
	UINT64 thing4;

} ReplayStatisticsOrSo;

typedef struct
{
	UINT32		threadUID;
	UINT32		threadId;
	Position	startingPosition;
	Position	endingPosition;
	Position	segmentsStartPosition;
	Position	segmentsEndPosition;

} ThreadInfo;

typedef struct
{
	GuestAddress startAddress;
	UINT64 size;
} MemoryRange;

typedef struct
{
	PVOID buffer;
	UINT64 requestedSize;

} TBufferView;

typedef union
{
	struct 
	{
		UINT64 unknown[9];
		UINT64 registers[30];
	} x64;

	BYTE data[2672];

} CROSS_PLATFORM_CONTEXT;

typedef struct
{
	BYTE data[8832];
} AVX_EXTENDED_CONTEXT;

typedef struct
{
	GuestAddress guestAddress;
	PVOID hostAddress;
	UINT64 size;

} MemoryBuffer;

typedef struct
{
	BYTE data[3200];
} EmulatorRegisters;