#pragma once
#include <Windows.h>

#include "InstrumentationCallbacks.h"

class ReplayEngine;

ReplayEngine *CreateReplayEngineWithTraceFile(const wchar_t *runFilePath);
UINT32 RegisterGlobalCallbacks(GlobalCallbacks * callbacks);