#pragma once
#include <Windows.h>
#define INITGUID
#include <guiddef.h>

#include "TTDBase.h"
#include "ExecutionState.h"

// ; GUID GUID_37196cf1_b27f_43ec_a063_4913b65c6035
DEFINE_GUID(CURSOR_GUID, 0x37196cf1, 0xb27f, 0x43ec, 0xa0, 0x63, 0x49, 0x13, 0xb6, 0x5c, 0x60, 0x35);

class Cursor
{
	PVOID ICursorInternalsVtable;
	PVOID ICursorVtable2;

	BYTE moreData[3832 - sizeof(PVOID) * 3];
public:
	ExecutionState *executionState;

public:
	virtual PVOID QueryMemoryRange(GuestAddress, QueryMemoryPolicy);
	virtual MemoryBuffer QueryMemoryBuffer(GuestAddress, TBufferView, QueryMemoryPolicy);
	virtual UINT32 QueryMemoryBufferWithRanges(GuestAddress, PVOID, unsigned __int64, MemoryRange *, QueryMemoryPolicy);
	virtual UINT32 SetDefaultMemoryPolicy(QueryMemoryPolicy);
	virtual QueryMemoryPolicy GetDefaultMemoryPolicy(void);
	virtual UINT32 UnsafeGetReplayEngine(_GUID const &);
	virtual UINT32 UnsafeAsInterface(_GUID const &);
	virtual UINT32 UnsafeAsInterface2(_GUID const &);
	virtual PVOID GetThreadInfo(ThreadId = CURRENT_TID);
	virtual PVOID GetTebAddress(ThreadId = CURRENT_TID);
	virtual Position & GetPosition(ThreadId = CURRENT_TID);
	virtual Position & GetPreviousPosition(ThreadId = CURRENT_TID);
	virtual GuestAddress GetProgramCounter(ThreadId = CURRENT_TID);
	virtual GuestAddress GetStackPointer(ThreadId = CURRENT_TID);
	virtual CROSS_PLATFORM_CONTEXT GetCrossPlatformContext(ThreadId = CURRENT_TID);
	virtual PVOID GetAvxExtendedContext(ThreadId = CURRENT_TID);
	virtual UINT32 GetModuleCount(void);
	virtual PVOID GetModuleList(void);
	virtual UINT32 GetThreadCount(void);
	virtual PVOID GetThreadList(void);
	virtual UINT32 SetEventMask(EventMask);
	virtual EventMask GetEventMask(void);
	virtual UINT32 SetExceptionMask(ExceptionMask);
	virtual ExceptionMask GetExceptionMask(void);
	virtual UINT32 SetReplayFlags(ReplayFlags);
	virtual ReplayFlags GetReplayFlags(void);
	virtual UINT32 AddWatchpoint(WatchpointData const &);
	virtual UINT32 RemoveWatchpoint(WatchpointData const &);
	virtual UINT32 Clear(void);
	virtual VOID SetPosition(Position &);
	virtual UINT32 SetPositionOnThread(UniqueThreadId, Position const &);
	//virtual UINT32 SetWatchpointCallback(bool(*)(unsigned __int64, TTD::Replay::ICursorView::WatchpointResult const &, TTD::Replay::IThreadView const *), unsigned __int64);
	virtual UINT32 SetWatchpointCallback(bool(*)(unsigned __int64, PVOID const &, PVOID const), unsigned __int64);
	virtual UINT32 SetReplayProgressCallback(void(*)(unsigned __int64, Position const &), unsigned __int64);
	virtual ReplayStatisticsOrSo ReplayForward(Position &, StepCount = -1);
	virtual ReplayStatisticsOrSo ReplayBackward(Position &, StepCount = -1);
	virtual UINT32 InterruptReplay(void);
	virtual UINT32 GetInternals(void);
	virtual UINT32 GetInternals2(void);
	//virtual ~Cursor();
	virtual UINT32 Destroy(void);
};

