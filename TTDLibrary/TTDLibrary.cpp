// TTDLibrary.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <Windows.h>
#include <string>
#include <stdio.h>
#define INITGUID
#include <guiddef.h>
#include <iostream>

#include "TTDInit.h"
#include "TTDLibrary.h"
#include "ReplayEngine.h"
#include "Cursor.h"
#include "ExecutionState.h"
#include "ActiveGuestContext.h"
#include "NirvanaVirtualCPU.h"
#include "InstrumentationCallbacks.h"

using namespace std;

Cursor *g_cursor = NULL;
UINT32 g_mainThreadId = 0;

int main()
{
	testTTD();
}

void testTTD()
{
	ReplayEngine *replayEngine = CreateReplayEngineWithTraceFile(L"notepad01.run");
	ThreadInfo *threadInfos = replayEngine->GetThreadList();
	ThreadId mainThreadId = threadInfos[0].threadId;

	Cursor *cursor = replayEngine->NewCursor(&CURSOR_GUID);
	g_cursor = cursor;

	GlobalCallbacks callbacks = { 0 };
	callbacks.MemoryReadCallback = MemoryReadCallback;
	callbacks.MemoryWriteCallback = MemoryWriteCallback;
	callbacks.InstructionTranslationCallback = InstructionTranslationCallback;
	RegisterGlobalCallbacks(&callbacks);

	cursor->SetPosition(replayEngine->GetFirstPosition());
	
	printf("Initial Position: %llx:%llx \n", cursor->GetPosition().sequencePoint, cursor->GetPosition().steps);
	printf("Initial RIP: %p \n", cursor->GetProgramCounter());

	/*char memoryBuffer[500] = { 0 };
	TBufferView bufferView = { 0 };
	bufferView.buffer = &memoryBuffer;
	bufferView.requestedSize = 500;
	MemoryBuffer memoryInfo2 = g_cursor->QueryMemoryBuffer(cursor->GetProgramCounter(), bufferView, 0);*/

	Position somewhereInTheFuture = { 0x20, 0x1E };
	cursor->ReplayForward(MaxPosition, 1);
	printf("Position is now: %llx:%llx \n", cursor->GetPosition().sequencePoint, cursor->GetPosition().steps);
	printf("RIP is now: %p \n", cursor->GetProgramCounter());
}

TranslationCallbackResult * InstructionTranslationCallback(TranslationCallbackResult *result, NirvanaVirtualCPU *virtualCpu, 
														   GuestAddress instructionAddress, UINT64 unknown, UINT64 instructionSize)
{
	ExecutionState *executionState = (ExecutionState *)((BYTE *)virtualCpu->contextMaybe - 128);
	Position currentPosition = executionState->GetPosition();
	currentPosition.steps += virtualCpu->GetInstructionCount();

	printf("RIP = %p (VCPU=%p) \n", instructionAddress, virtualCpu);
	printf("Position: %llx:%llx \n", currentPosition.sequencePoint, currentPosition.steps);

	/*UINT64 registerBuffer = -1;
	virtualCpu->GetRegister(rax, &registerBuffer, sizeof(registerBuffer));
	printf("RAX = %llx \n", registerBuffer);

	char memoryBuffer[500] = { 0 };
	TBufferView bufferView = { 0 };
	bufferView.buffer = &memoryBuffer;
	bufferView.requestedSize = 500;
	MemoryBuffer memoryInfo = executionState->QueryMemoryBuffer((GuestAddress)((UINT64)instructionAddress), bufferView);*/


	result->userCallback = NULL;
	return result;
}

VOID MemoryReadCallback(NirvanaVirtualCPU *nirvanaVirtualCpu, GuestAddress address, PVOID somePointer, UINT64 readSize)
{
	/*UINT64 rip = nirvanaVirtualCpu->GetPC64();
	printf("RIP = %p (VCPU=%p) \n", rip, nirvanaVirtualCpu);
	printf("Memory at %p with size %llu was read.\n", address, readSize);*/
}

VOID MemoryWriteCallback(NirvanaVirtualCPU *virtualCpu, GuestAddress address, PVOID somePointer, UINT64 writeSize)
{
	//printf("RIP = %p (VCPU=%p) \n", virtualCpu->GetPC64(), virtualCpu);
	//printf("Memory at %p with size %llu was written to.\n", address, writeSize);
}
